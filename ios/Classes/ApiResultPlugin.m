#import "ApiResultPlugin.h"
#if __has_include(<api_result/api_result-Swift.h>)
#import <api_result/api_result-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "api_result-Swift.h"
#endif

@implementation ApiResultPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftApiResultPlugin registerWithRegistrar:registrar];
}
@end
