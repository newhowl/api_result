import 'api_result.dart';

///
/// when complete
///
typedef ApiResultSuccessCallback = Future<void> Function(dynamic fetch, dynamic data);
typedef ApiResultFailedCallback = Future<void> Function(ApiResultType type, String msg);
typedef ApiResultSuccessAtNullFetchCallback = Future<void> Function();

///
/// make
///
typedef MapToDynamicCallback = dynamic Function(Map mdp);