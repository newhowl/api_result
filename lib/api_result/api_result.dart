import 'package:common_util/logger/logger.dart';
import 'package:common_util/util/map_util.dart';
import 'package:common_util/util/object_util.dart';
import 'package:flutter/material.dart';

import 'api_result_typedef.dart';

enum ApiResultType {
  init,
  success,
  failed,
  failed_upload_file,
  failed_fetch_data,
  failed_not_available,
  timeout,
  exception,
  exception_server,
  exception_local,
  exception_function,
  null_argument,
  null_data,
  null_result,
  permission,
  blacklist,
  impossible_self,
  wrong_session,
  wrong_access_token,
  wrong_argument,
  wrong_valid_date,
}

typedef OnCacheCheckCallback = Future<bool> Function();
typedef OnCacheGetCallback = Future<dynamic> Function();
typedef OnCacheFetchCallback = Future<dynamic> Function();
typedef OnCacheUpdate2Callback = Future<dynamic> Function(dynamic fetch);
typedef OnCacheUpdateDBCallback = Future<void> Function(dynamic fetch);

class ApiResult {
  ApiResultType type = ApiResultType.init;
  ApiResultType typeOfApi = ApiResultType.init;
  dynamic fetch;
  dynamic data;
  var refMsg = '';
  dynamic lastSnapshot;

  void dispose() {
    fetch = null;
    data = null;
  }

  int getCodeByType() {
    var code = 0;
    switch(type) {
      case ApiResultType.init: code = 0; break;
      case ApiResultType.success: code = 200; break;
      case ApiResultType.failed: code = 300; break;
      case ApiResultType.failed_upload_file: code = 301; break;
      case ApiResultType.failed_fetch_data: code = 302; break;
      case ApiResultType.failed_not_available: code = 303; break;
      case ApiResultType.timeout: code = 400; break;
      case ApiResultType.exception: code = 500; break;
      case ApiResultType.exception_server: code = 501; break;
      case ApiResultType.exception_local: code = 502; break;
      case ApiResultType.exception_function: code = 503; break;
      case ApiResultType.null_argument: code = 600; break;
      case ApiResultType.null_data: code = 601; break;
      case ApiResultType.null_result: code = 602; break;
      case ApiResultType.permission: code = 700; break;
      case ApiResultType.blacklist: code = 800; break;
      case ApiResultType.impossible_self: code = 900; break;
      case ApiResultType.wrong_session: code = 1000; break;
      case ApiResultType.wrong_access_token: code = 1001; break;
      case ApiResultType.wrong_argument: code = 1002; break;
      case ApiResultType.wrong_valid_date: code = 1003; break;
    }
    return code;
  }

  String getCodeStringByType() {
    var code = '';
    switch(type) {
      case ApiResultType.init: code = 'init'; break;
      case ApiResultType.success: code = 'success'; break;
      case ApiResultType.failed: code = 'failed'; break;
      case ApiResultType.failed_upload_file: code = 'failed_upload_file'; break;
      case ApiResultType.failed_fetch_data: code = 'failed_fetch_date'; break;
      case ApiResultType.failed_not_available: code = 'failed_not_available'; break;
      case ApiResultType.timeout: code = 'timeout'; break;
      case ApiResultType.exception: code = 'exception'; break;
      case ApiResultType.exception_server: code = 'exception_server'; break;
      case ApiResultType.exception_local: code = 'exception_local'; break;
      case ApiResultType.exception_function: code = 'exception_function'; break;
      case ApiResultType.null_argument: code = 'null_argument'; break;
      case ApiResultType.null_data: code = 'null_data'; break;
      case ApiResultType.null_result: code = 'null_result'; break;
      case ApiResultType.permission: code = 'permission'; break;
      case ApiResultType.blacklist: code = 'blacklist'; break;
      case ApiResultType.impossible_self: code = 'impossible_self'; break;
      case ApiResultType.wrong_session: code = 'wrong_session'; break;
      case ApiResultType.wrong_access_token: code = 'wrong_access_token'; break;
      case ApiResultType.wrong_argument: code = 'wrong_argument'; break;
      case ApiResultType.wrong_valid_date: code = 'wrong_valid_date'; break;
    }
    return code;
  }

  ApiResult setApiResult({
    ApiResultType? type, dynamic fetch, dynamic data, String? msg,
  }) {
    if (isExists(type)) this.type = type!;
    if (isExists(fetch)) this.fetch = fetch;
    if (isExists(data)) this.data = data;
    if (isExists(msg)) refMsg = msg!;
    return this;
  }

  ApiResult setApiResultOfApi({
    ApiResultType? type,
  }) {
    if (isExists(type)) typeOfApi = type!;
    return this;
  }

  bool checkSuccess() {
    return (ApiResultType.success == type);
  }

  bool checkSuccessOfApi() {
    return (ApiResultType.success == typeOfApi);
  }

  bool checkInit() {
    return (ApiResultType.init == type);
  }

  Future<dynamic> makeDataByMap({MapToDynamicCallback? onMake}) async {
    var isCorrect = checkSuccess() && isExists(fetch) && (fetch is Map);
    if (isCorrect) {
      data = onMake?.call(fetch);
      return data;
    }
    return null;
  }

  void addResultMap(String key, dynamic value) {
    var isMap = fetch is Map;
    if (isMap) {
      setItemFromMap(fetch, key, value);
    }
  }

  Future<dynamic> whenGet({
    @required String? title,
    OnCacheGetCallback? onGetCache,
    OnCacheCheckCallback? onExpireCache,
    OnCacheGetCallback? onGetDB,
    OnCacheFetchCallback? onGetApi,
    OnCacheUpdate2Callback? onUpdateCache,
    OnCacheUpdateDBCallback? onUpdateDB,
    bool priorityCache = false,
  }) async {
    dynamic cache;
    dynamic map;
    dynamic fetch;

    if (priorityCache) {
      cache = await onGetCache?.call();
      if (isExists(cache)) {
        setApiResult(type: ApiResultType.success, fetch: cache, data: cache);
        return cache;
      }
    }
    map = await onGetDB?.call();
    if (isExists(map)) {
      setApiResult(type: ApiResultType.success, fetch: map, data: map);
      cache = await onUpdateCache?.call(map);
      setApiResult(data: cache);
    }
    if (isExists(cache)) {
      onGetApi?.call().then((value) async {
        fetch = value;
        if (isExists(fetch) && checkSuccessOfApi()) {
          if (isExists(onUpdateDB)) {
            onUpdateDB?.call(fetch);
          }
          if (isExists(onUpdateCache)) {
            cache = await onUpdateCache?.call(fetch);
            setApiResult(data: cache);
          }
        }
        if (!checkSuccessOfApi()) {
          logHelper.w('failed unawaited api > $title >');
        }
      });
    } else {
      fetch = await onGetApi?.call();
      if (isExists(fetch) && checkSuccessOfApi()) {
        if (isExists(onUpdateDB)) {
          onUpdateDB?.call(fetch);
        }
        if (isExists(onUpdateCache)) {
          cache = await onUpdateCache?.call(fetch);
          setApiResult(data: cache);
        }
      }
      if (!checkSuccessOfApi()) {
        logHelper.w('failed awaited api > $title >');
      }
    }
    return cache;
  }

  Future<void> whenComplete({
    ApiResultSuccessCallback? onSuccess,
    ApiResultFailedCallback? onFailed,
    ApiResultSuccessAtNullFetchCallback? onSuccessAtNullFetch,
  }) async {
    var isSuccess = checkSuccess();
    if (isSuccess) {
      if (isExists(onSuccessAtNullFetch) && isNotExists(fetch)) {
        await onSuccessAtNullFetch?.call();
      } else {
        await onSuccess?.call(fetch, data);
      }
    } else {
      var errMsg = getCodeStringByType() + ' ' +  refMsg;
      await onFailed?.call(type, errMsg);
    }
  }

  @override
  String toString() {
    return 'toString > User > '
        'type: $type '
        'typeOfApi: $typeOfApi '
        'fetch: $fetch '
        'data: $data '
        'refMsg: $refMsg '
    ;
  }
}