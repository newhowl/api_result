import 'package:api_result/api_result/api_result.dart';

///
/// doProcessWithApiResult
///
typedef LoadingStatePrefixCallback = Future<bool> Function();
typedef LoadingStateProcessCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateProcessedCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateUpdateCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateSuccessCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateSuccessAtNullFetchCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateFailedCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});
typedef LoadingStateSetResultCallback = Future<bool> Function();
typedef LoadingStateCatchCallback = Future<void> Function();
typedef LoadingStateFinallyCallback = Future<void> Function(ApiResult apiResult, {bool isRefresh});

///
/// doProcess
///
typedef FutureBoolCallback = Future<bool> Function();
typedef BoolCallback = bool Function();
typedef FutureCallback = Future<void> Function();

