
import 'dart:async';

import 'package:flutter/services.dart';

class ApiResult {
  static const MethodChannel _channel =
      const MethodChannel('api_result');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
